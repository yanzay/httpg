// Static file server.
//
// Serves static files from the given directory.
package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/valyala/fasthttp"
)

var (
	port = flag.Int("p", 8080, "TCP port to listen to")
)

func main() {
	flag.Parse()

	fs := &fasthttp.FS{
		Root:               ".",
		IndexNames:         []string{"index.html"},
		GenerateIndexPages: true,
	}
	fsHandler := fs.NewRequestHandler()

	log.Printf("Starting HTTP server on http://localhost:%d", *port)
	err := fasthttp.ListenAndServe(fmt.Sprintf("localhost:%d", *port), fsHandler)
	if err != nil {
		log.Fatalf("error in ListenAndServe: %s", err)
	}
}
